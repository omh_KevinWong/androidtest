package omh.testapplication;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

/* TODO:
    * Task 1) Optimus Prime!
    * --- Description ---
    * Optimize the view layout hierarchy and do whatever you can to make this activity launch faster on the 1st load.
    * Please take note that the UI should look the same as before
    * Edit activity_task1.xml and content_complex_view.xml
    *
    * */


public class Task1 extends AppCompatActivity {

    @Override
    protected void onResume() {
        super.onResume();
        long timeTaken = MApplication.getInstance().stopActivityTransitionTimer();
        new AlertDialog.Builder(Task1.this)
                .setTitle("Completed")
                .setMessage("Time taken for activity to be launched: " + timeTaken + "ms")
                .setCancelable(false)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task1);
        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_task1_toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.activity_task1_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Copyright © 2019 Ohmyhome Pte Ltd, All rights reserved", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

}
